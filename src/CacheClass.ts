import CacheValue from "./CacheValue"

export default class CacheClass<K, V> {
  /**
   * Коллекция для хранения пар ключ-значение.
   * @private
   */
  private readonly cacheMap: Map<K, CacheValue<V>>

  /**
   * Максимальное количество элементов
   * @private
   */
  private readonly maxSize: number

  /**
   * Время жизни элемента кэша (миллисекунды)
   * @private
   */
  private readonly ttl: number

  /**
   * Конструктор для CacheClass
   * @param maxSize Максимальное количество элементов
   * @param ttl Время жизни элемента кэша (миллисекунды)
   */
  constructor(maxSize: number, ttl: number) {
    this.cacheMap = new Map<K, CacheValue<V>>()
    this.maxSize = maxSize
    this.ttl = ttl
  }

  /**
   * Добавляет элемент в кэш или обновляет существующий.
   * Если кэш заполнен, удаляет наименее используемый элемент.
   * @param key Ключ, по которому сохраняется значение.
   * @param value Значение для сохранения в кэше.
   */
  set(key: K, value: V): void {
    if (this.cacheMap.size >= this.maxSize) {
      let leastUsedKey: K | null = null
      let leastUsedCount: number = Infinity

      for (const [currentKey, cacheValue] of this.cacheMap) {
        const currentUsage: number = cacheValue.getUsageCount()
        if (currentUsage < leastUsedCount) {
          leastUsedCount = currentUsage
          leastUsedKey = currentKey
        }
      }

      if (leastUsedKey !== null) {
        this.cacheMap.delete(leastUsedKey)
      }
    }

    const expiry: number = Date.now() + this.ttl
    this.cacheMap.set(key, new CacheValue(value, expiry))
  }

  /**
   * Возвращает значение из кэша по ключу, если оно существует и не истекло.
   * @param key Ключ, по которому запрашивается значение.
   * @returns Значение, соответствующее ключу, или undefined, если ключ не найден или время жизни истекло.
   */
  get(key: K): V | undefined {
    const item = this.cacheMap.get(key)
    if (!item) return undefined

    if (Date.now() > item.getExpiry()) {
      this.cacheMap.delete(key)
      return undefined
    }
    return item.getValue()
  }

  /**
   * Очищает кэш от всех элементов.
   */
  clear(): void {
    this.cacheMap.clear()
  }
}
