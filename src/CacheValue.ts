export default class CacheValue<V> {
  /**
   * Хранимое значение.
   * @private
   */
  private readonly value: V
  /**
   * Время истечения срока действия значения.
   * @private
   */
  private readonly expiry: number
  /**
   * Счётчик, отслеживающий, сколько раз значение было запрошено.
   * @private
   */
  private usageCount: number

  /**
   * Конструктор для создания экземпляра CacheValue.
   * @param value Значение, хранимое в кэше.
   * @param expiry Время истечения срока действия значения (в миллисекундах).
   */
  constructor(value: V, expiry: number) {
    this.value = value
    this.expiry = expiry
    this.usageCount = 0
  }

  /**
   * Возвращает хранимое значение и увеличивает счётчик использования.
   * @returns Хранимое значение.
   */
  public getValue(): V {
    this.usageCount++
    return this.value
  }

  /**
   * Возвращает время истечения срока действия значения.
   * @returns Время истечения срока действия значения в миллисекундах.
   */
  public getExpiry(): number {
    return this.expiry
  }

  /**
   * Возвращает количество раз, когда было запрошено значение.
   * @returns Количество использований значения.
   */
  public getUsageCount(): number {
    return this.usageCount
  }
}
