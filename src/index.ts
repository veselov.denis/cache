import CacheClass from "./CacheClass"

// Инициализация кэша с максимальным размером 3 и временем жизни элементов 5 секунд
const maxSize: number = 3
const ttl: number = 5000

const cache: CacheClass<string, any> = new CacheClass(maxSize, ttl)

// Добавление различных элементов кэша
cache.set("key1", "SimpleString")
cache.set("key2", { key: "value" })
cache.set("key3", [1, 2, 3])

console.log("Начальное состояние кэша:")
console.log("key1:", cache.get("key1"))
console.log("key2:", cache.get("key2"))
console.log("key3:", cache.get("key3"))

// Имитация повторного доступа к определенным ключам
console.log(
  "\nИмитируем запрос к элементам key1 and key3 несколько раз, чтобы увеличить значение их использования..."
)
Array(5)
  .fill(null)
  .forEach(() => cache.get("key1"))

Array(3)
  .fill(null)
  .forEach(() => cache.get("key3"))

// Добавление нового элемента, что приведет к удалению одного из старых
console.log(
  "\nДобавим новый ключ (key4: NewValue), при этом элемент с наименьшим количеством использований (key2) должен быть удален..."
)
cache.set("key4", "NewValue")

console.log("\nНовое состояние кэша:")
console.log("key1:", cache.get("key1")) // Должен быть доступен
console.log("key2 (должен быть недоступен):", cache.get("key2")) // Может быть удален из кэша
console.log("key3:", cache.get("key3")) // Может быть удален из кэша
console.log("key4:", cache.get("key4")) // Должен быть доступен

// Проверка поведения кэша при попытке доступа к устаревшим данным
console.log("\nОжидаем 6 секунд, чтобы у всех элементов истекло время жизни...")
setTimeout(() => {
  console.log(
    "\nПробуем получить доступ к ранее заданным элементам (все они должны быть недоступны):"
  )
  console.log("key1:", cache.get("key1")) // Должен быть undefined, если время жизни истекло
  console.log("key2:", cache.get("key2")) // Аналогично
  console.log("key3:", cache.get("key3")) // Аналогично
  console.log("key4:", cache.get("key4")) // Аналогично
}, 6000)
